using System.ComponentModel.DataAnnotations;
using WebApi.Db;

namespace WebApi.Models
{
    public class Customer: BaseEntity
    {   
        [Required]
        public string Firstname { get; init; }

        [Required]
        public string Lastname { get; init; }
    }
}