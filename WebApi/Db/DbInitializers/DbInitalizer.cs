﻿using System.Threading.Tasks;
using System.Linq;
using WebApi.Db.Contexts;
using System.Collections.Generic;
using WebApi.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DbInitializers
{
    public class DbInitializer : IDbInitializer
    {
        private readonly SQLiteDbContext _sqliteDbContext;

        public DbInitializer(SQLiteDbContext dbContext)
        {
            _sqliteDbContext = dbContext;
        }

        public void InitializeDb()
        {
            _sqliteDbContext.Database.EnsureCreated();

            if (!_sqliteDbContext.Customers.Any())
            {
                _sqliteDbContext.Customers.AddRange(GenerateInitialCustomers());
            }

            _sqliteDbContext.SaveChanges();
        }

        private List<Customer> GenerateInitialCustomers() =>
            new List<Customer>()
            {
                new Customer()
                {
                    Id = 1,
                    Firstname = "Александр",
                    Lastname = "Темный"
                },
                new Customer()
                {
                    Id = 2,
                    Firstname = "Андрей",
                    Lastname = "Светлый"
                },
                new Customer()
                {
                    Id = 3,
                    Firstname = "Николай",
                    Lastname = "Серый"
                },
            };
    }

}
