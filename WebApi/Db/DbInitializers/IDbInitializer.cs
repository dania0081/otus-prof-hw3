﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DbInitializers
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
