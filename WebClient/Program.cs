﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static readonly HttpClient client = new HttpClient();
        
        private static Random random = new Random((int)DateTime.Now.Ticks);

        private const string serviceURL = "https://localhost:5001/customers/";

        static async Task Main(string[] args)
        {
            PrintCommandList();

            var exit = false;
            
            do
            {
                Console.WriteLine("Введите команду");

                var command = Console.ReadLine();
                switch (command.ToLower())
                {
                    case "get":
                        await ExecutetGetCustomerCommandAsync();
                        break;
                    case "add":
                        await ExecuteAddCustomerCommandAsync();
                        break;
                    case "exit":
                        exit = true;
                        break;
                    default:
                        PrintCommandList();
                        break;
                }

            }
            while (!exit);

            Console.WriteLine("Пока!");
            Thread.Sleep(3000);
        }

        private static async Task ExecutetGetCustomerCommandAsync()
        {
            try
            {
                Console.WriteLine("Введите идентификатор клиента");
                var customerIdStr = Console.ReadLine();

                if (long.TryParse(customerIdStr, out long customerId))
                {
                    await GetCustomerById(customerId);
                }
                else
                {
                    Console.WriteLine("ID не является long integer");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private static async Task ExecuteAddCustomerCommandAsync()
        {
            var customerRequest = RandomCustomer();
            HttpContent content = new StringContent(JsonConvert.SerializeObject(customerRequest), Encoding.UTF8, "application/json");
            var responce = await client.PostAsync("https://localhost:5001/customers", content);
            
            if (responce != null && responce.IsSuccessStatusCode)
            {
                var result = await responce.Content.ReadAsStringAsync();
                if (int.TryParse(result, out int customerId))
                {
                    await GetCustomerById(customerId);
                }
            }
        }

        private static async Task GetCustomerById(long customerId)
        {
            var httpResponse = await client.GetAsync($"{serviceURL}{customerId}");

            if (httpResponse.IsSuccessStatusCode)
            {
                var json = httpResponse.Content.ReadAsStringAsync().Result;
                var customer = JsonConvert.DeserializeObject<Customer>(json);
                Console.WriteLine($"Идентификатор: {customer.Id}; Имя: {customer.Firstname}; Фамилия: {customer.Lastname}");
            }
            else
            {
                Console.WriteLine(httpResponse.StatusCode);
            }
        }

        private static void PrintCommandList()
        {
            Console.WriteLine("Список команд:");
            Console.WriteLine("Get - получить данные клиента по идентификатору");
            Console.WriteLine("Add - добавить случайного клиента");
            Console.WriteLine("Exit - выход");
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest(RandomString(5), RandomString(5));
        }

        private static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
           
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}